package ru.tsc.bagrintsev.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectListRequest;
import ru.tsc.bagrintsev.tm.dto.response.project.ProjectListResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class ProjectListListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String description() {
        return "Print project list.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectListListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.SORT);
        System.out.println(Arrays.toString(WBSSort.values()));
        @Nullable final String sortValue = TerminalUtil.nextLine();
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortValue(sortValue);
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        @Nullable final List<ProjectDto> projects = response.getProjects();
        if (projects != null) {
            projects.forEach(System.out::println);
        }
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

}
