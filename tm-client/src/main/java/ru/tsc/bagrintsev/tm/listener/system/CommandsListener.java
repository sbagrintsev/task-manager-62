package ru.tsc.bagrintsev.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class CommandsListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print application interaction commands.";
    }

    @Override
    @SneakyThrows
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        repository.stream()
                .filter(c -> !c.name().isEmpty())
                .forEach(c -> System.out.printf("%-35s%s\n", c.name(), c.description()));
    }

    @EventListener(condition = "@commandsListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@commandsListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-cmd";
    }

}
