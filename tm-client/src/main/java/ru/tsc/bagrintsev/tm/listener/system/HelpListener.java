package ru.tsc.bagrintsev.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class HelpListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print application help.";
    }

    @Override
    @SneakyThrows
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        System.out.println("[While Running                       | Command Line             ]");
        System.out.println("[------------------------------------|--------------------------]");
        repository.forEach(System.out::println);
    }

    @EventListener(condition = "@helpListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@helpListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-h";
    }

}
