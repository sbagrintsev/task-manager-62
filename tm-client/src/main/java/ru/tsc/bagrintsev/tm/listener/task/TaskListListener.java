package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskListRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskListResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskListListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Print task list.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskListListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.SORT);
        System.out.println(Arrays.toString(WBSSort.values()));
        @Nullable final String sortValue = TerminalUtil.nextLine();
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSortValue(sortValue);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        @Nullable final List<TaskDto> tasks = response.getTasks();
        if (tasks != null) {
            tasks.forEach(System.out::println);
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

}
