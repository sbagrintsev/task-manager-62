package ru.tsc.bagrintsev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class EventMessage implements Serializable {

    private final String id = UUID.randomUUID().toString();

    private final String date = DateUtil.toString(new Date());

    private String eventType = "";

    private Object object = null;

    private String tableName = "other";

    public EventMessage(
            @NotNull final Object object,
            @NotNull final String eventType
    ) {
        this.object = object;
        this.eventType = eventType;
    }

}
