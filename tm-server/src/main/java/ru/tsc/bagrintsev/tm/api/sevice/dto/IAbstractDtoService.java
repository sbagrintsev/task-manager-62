package ru.tsc.bagrintsev.tm.api.sevice.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.List;

public interface IAbstractDtoService<M extends AbstractDtoModel> {

    void clearAll();

    @NotNull
    List<M> findAll();

    @NotNull
    Collection<M> set(@NotNull final Collection<M> records);

    long totalCount();

}
