package ru.tsc.bagrintsev.tm.api.sevice.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.EmailIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.system.EmptyArgumentException;
import ru.tsc.bagrintsev.tm.exception.user.*;

import java.security.GeneralSecurityException;

public interface IUserDtoService extends IAbstractDtoService<UserDto> {

    @NotNull
    UserDto checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws PasswordIsIncorrectException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, UserNotFoundException;

    @NotNull
    UserDto create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, PasswordIsIncorrectException, LoginIsIncorrectException, LoginAlreadyExistsException;

    @NotNull
    UserDto findByEmail(@Nullable final String email) throws EmailIsEmptyException, UserNotFoundException;

    @NotNull
    UserDto findByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException;

    @NotNull
    UserDto findOneById(@Nullable String id) throws IdIsEmptyException, UserNotFoundException;

    boolean isEmailExists(@Nullable final String email);

    boolean isLoginExists(@Nullable final String login);

    void lockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException;

    @NotNull
    UserDto removeByLogin(@Nullable final String login) throws LoginIsIncorrectException, IdIsEmptyException, UserNotFoundException;

    @NotNull
    UserDto setParameter(
            @Nullable final UserDto user,
            @NotNull final EntityField paramName,
            @Nullable final String paramValue
    ) throws EmailAlreadyExistsException, UserNotFoundException, IncorrectParameterNameException, EmptyArgumentException;

    @NotNull
    UserDto setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws GeneralSecurityException, PasswordIsIncorrectException, IdIsEmptyException, AccessDeniedException, LoginIsIncorrectException, UserNotFoundException;

    @NotNull
    UserDto setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws IncorrectRoleException, LoginIsIncorrectException, UserNotFoundException;

    void unlockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException;

    @NotNull
    UserDto updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws IdIsEmptyException, UserNotFoundException;

}
