package ru.tsc.bagrintsev.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.ISenderService;
import ru.tsc.bagrintsev.tm.enumerated.EventType;

@Component
@NoArgsConstructor
public class EntityListener implements
        PostInsertEventListener,
        PostUpdateEventListener,
        PostDeleteEventListener {

    @Nullable
    private ISenderService senderService;

    public EntityListener(@NotNull final ISenderService senderService) {
        this.senderService = senderService;
    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {
        sendMessage(event.getEntity(), EventType.DELETE);
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        sendMessage(event.getEntity(), EventType.INSERT);
    }

    @Override
    public void onPostUpdate(PostUpdateEvent event) {
        sendMessage(event.getEntity(), EventType.UPDATE);
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    @Override
    public boolean requiresPostCommitHandling(EntityPersister persister) {
        return PostInsertEventListener.super.requiresPostCommitHandling(persister);
    }

    private void sendMessage(
            @NotNull final Object entity,
            @NotNull final EventType eventType
    ) {
        if (senderService == null) return;
        senderService.createMessage(entity, eventType.toString());
    }

}
