package ru.tsc.bagrintsev.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.AbstractWBSModel;

@Repository
@Scope("prototype")
public interface AbstractWBSRepository<M extends AbstractWBSModel> extends AbstractUserOwnedRepository<M> {

}
