package ru.tsc.bagrintsev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    void deleteByLogin(@NotNull final String login);

    boolean existsByEmail(@NotNull final String email);

    boolean existsByLogin(@NotNull final String login);

    @Nullable
    User findFirstByEmail(@NotNull final String email);

    @Nullable
    User findFirstByLogin(@NotNull final String login);

    @Modifying
    @Query(value = "UPDATE User SET role = :role WHERE login = :login")
    void setRole(
            @NotNull @Param("login") final String login,
            @NotNull @Param("role") final Role role);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE User SET passwordHash = :password, passwordSalt = :salt WHERE login = :login")
    void setUserPassword(
            @NotNull @Param("login") final String login,
            @NotNull @Param("password") final String password,
            final @Param("salt") byte @NotNull [] salt);

}
