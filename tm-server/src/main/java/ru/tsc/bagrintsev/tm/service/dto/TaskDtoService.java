package ru.tsc.bagrintsev.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskDtoService;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.repository.dto.TaskDtoRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskDtoService extends AbstractUserOwnedDtoService<TaskDto> implements ITaskDtoService {

    @NotNull
    private final TaskDtoRepository taskRepository;

    @Override
    @Transactional
    public @NotNull TaskDto add(
            @Nullable final String userId,
            @Nullable final TaskDto task
    ) throws ModelNotFoundException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (task == null) throw new ModelNotFoundException();
        task.setUserId(userId);
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public @NotNull TaskDto changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable TaskDto task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (Status.IN_PROGRESS.equals(status)) {
            task.setStatus(status);
            task.setDateStarted(new Date());
        } else if (Status.COMPLETED.equals(status)) {
            task.setStatus(status);
            task.setDateFinished(new Date());
        } else if (Status.NOT_STARTED.equals(status)) {
            task.setStatus(status);
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void clearAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public @NotNull TaskDto create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @Override
    @Transactional
    public @NotNull TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public @NotNull List<TaskDto> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        List<TaskDto> list = taskRepository.findAllByUserId(userId);
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public @NotNull List<TaskDto> findAll(
            @Nullable final String userId,
            @Nullable final WBSSort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final String order = getQueryOrder(sort);
        @Nullable final List<TaskDto> list = taskRepository.findAllByUserId(userId, org.springframework.data.domain.Sort.by(order));
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        List<TaskDto> list = taskRepository.findAllByUserIdAndProjectId(userId, projectId);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public TaskDto findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable TaskDto task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @Transactional
    public @NotNull TaskDto removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable TaskDto task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        taskRepository.deleteByUserIdAndId(userId, id);
        return task;
    }

    @Override
    @Transactional
    public @NotNull Collection<TaskDto> set(@NotNull final Collection<TaskDto> tasks) {
        if (tasks.isEmpty()) return tasks;
        return taskRepository.saveAllAndFlush(tasks);
    }

    @Override
    @Transactional
    public @NotNull TaskDto setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws TaskNotFoundException {
        taskRepository.setProjectId(userId, taskId, projectId);
        @Nullable final TaskDto task = taskRepository.findByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public long totalCount() {
        return taskRepository.count();
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public @NotNull TaskDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable TaskDto task;
        taskRepository.updateById(userId, id, name, description);
        task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

}
