package ru.tsc.bagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserOwnedService;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.model.AbstractWBSModel;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractWBSModel> extends AbstractService<M> implements IUserOwnedService<M> {

    @Override
    public abstract M add(@Nullable String userId, M record) throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException;

    @Override
    public abstract M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException, ModelNotFoundException;

    @Override
    public abstract void clear(@Nullable String userId) throws IdIsEmptyException;

    @Override
    public abstract M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException, UserNotFoundException;

    @Override
    public abstract M create(
            @Nullable String userId,
            @Nullable String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException;

    @Override
    public abstract boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException;

    @Override
    @NotNull
    public abstract List<M> findAll(@Nullable String userId) throws IdIsEmptyException;

    @Override
    @NotNull
    public abstract List<M> findAll(
            @Nullable String userId,
            @Nullable WBSSort sort
    ) throws IdIsEmptyException;

    @Override
    public abstract M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    @NotNull
    protected String getQueryOrder(@NotNull final WBSSort sort) {
        if (sort.equals(WBSSort.BY_NAME)) return "name";
        else if (sort.equals(WBSSort.BY_STATUS)) return "status";
        else if (sort.equals(WBSSort.BY_STARTED)) return "dateStarted";
        else return "dateCreated";
    }

    @Override
    public abstract M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    @Override
    @NotNull
    public abstract Collection<M> set(@NotNull Collection<M> records);

    @Override
    public abstract long totalCount(@Nullable String userId) throws IdIsEmptyException;

    @Override
    public abstract M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException;

}
