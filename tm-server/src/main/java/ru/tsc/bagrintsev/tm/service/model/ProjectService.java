package ru.tsc.bagrintsev.tm.service.model;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.model.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final UserRepository userRepository;

    @Override
    @Transactional
    public @NotNull Project add(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (project == null) throw new ModelNotFoundException();
        @Nullable final User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        projectRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    public @NotNull Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, ProjectNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable Project project = projectRepository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (Status.IN_PROGRESS.equals(status)) {
            project.setStatus(status);
            project.setDateStarted(new Date());
        } else if (Status.COMPLETED.equals(status)) {
            project.setStatus(status);
            project.setDateFinished(new Date());
        } else if (Status.NOT_STARTED.equals(status)) {
            project.setStatus(status);
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void clearAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    @Transactional
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @NotNull List<Project> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @Nullable final List<Project> list = projectRepository.findAllByUserId(userId);
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public @NotNull List<Project> findAll(
            @Nullable final String userId,
            @Nullable final WBSSort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final String order = getQueryOrder(sort);
        @Nullable final List<Project> list = projectRepository.findAllByUserId(userId, org.springframework.data.domain.Sort.by(order));
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable Project project = projectRepository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.deleteByUserIdAndId(userId, id);
        return project;
    }

    @Override
    @Transactional
    public @NotNull Collection<Project> set(@NotNull final Collection<Project> projects) {
        if (projects.isEmpty()) return projects;
        projectRepository.saveAll(projects);
        return projects;
    }

    @Override
    public long totalCount() {
        return projectRepository.count();
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public @Nullable Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        projectRepository.updateById(userId, id, name, description);
        return projectRepository.findByUserIdAndId(userId, id);
    }

}
