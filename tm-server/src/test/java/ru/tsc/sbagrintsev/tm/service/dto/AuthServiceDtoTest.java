package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthDtoService;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.service.dto.AuthDtoService;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;

@Category(DBCategory.class)
public final class AuthServiceDtoTest extends AbstractDtoTest {

    @NotNull
    private IAuthDtoService authService;

    @Nullable
    private String token;

    @Before
    public void setUp() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        authService = new AuthDtoService(userService, propertyService);
        userService.create("testLogin", "testPassword");
        token = null;
    }

    @After
    public void tearDown() throws AbstractException {
        userService.removeByLogin("testLogin");
    }

    @Test
    @Category(DBCategory.class)
    public void testSignIn() throws Exception {
        Assert.assertNull(token);
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
    }

    @Test
    @Category(DBCategory.class)
    public void testValidateToken() throws Exception {
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
        Assert.assertNotNull(authService.validateToken(token));
    }

}
