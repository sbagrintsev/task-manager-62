package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.enumerated.WBSSort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.util.Arrays;
import java.util.List;

import static ru.tsc.bagrintsev.tm.enumerated.Status.IN_PROGRESS;
import static ru.tsc.bagrintsev.tm.enumerated.Status.NOT_STARTED;

@Category(DBCategory.class)
public final class TaskDtoServiceTest extends AbstractDtoTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @Before
    public void setUp() {
        @NotNull final ProjectDto project1 = new ProjectDto();
        project1.setName("project1");
        project1.setId("project1");
        @NotNull final ProjectDto project2 = new ProjectDto();
        project2.setName("project2");
        project2.setId("project2");
        projectService.set(Arrays.asList(project1, project2));
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        taskService.add(userId, task);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(task.getId(), taskService.findAll().get(0).getId());
        Assert.assertEquals(userId, taskService.findAll().get(0).getUserId());
    }

    @Test
    @Category(DBCategory.class)
    public void testAddCollection() throws IdIsEmptyException {
        taskService.clearAll();
        @NotNull final TaskDto task1 = new TaskDto();
        task1.setUserId(userId);
        task1.setName("1");
        @NotNull final TaskDto task2 = new TaskDto();
        task2.setUserId(userId);
        task2.setName("2");
        @NotNull final List<TaskDto> taskList = Arrays.asList(task1, task2);
        taskService.set(taskList);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(2, taskService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testChangeStatusById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals(NOT_STARTED, taskService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(taskService.changeStatusById(userId, id, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, taskService.findOneById(userId, id).getStatus());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        task.setId("id1");
        task.setName("name1");
        @NotNull final TaskDto task2 = new TaskDto();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        @NotNull final TaskDto task3 = new TaskDto();
        task.setId("id3");
        task.setName("name3");
        taskService.add(userId2, task3);
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws AbstractException {
        taskService.clearAll();
        taskService.create(userId, "name1");
        taskService.create(userId, "name2", "description2");
        Assert.assertEquals(2, taskService.totalCount(userId));
        Assert.assertEquals("name1", taskService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("description2", taskService.findAll(userId, WBSSort.BY_NAME).get(1).getDescription());
        Assert.assertEquals(userId, taskService.findAll().get(0).getUserId());
        Assert.assertEquals(userId, taskService.findAll().get(1).getUserId());
    }

    @Test
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, "id1"));
        Assert.assertFalse(taskService.existsById(userId, "id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws AbstractException {
        taskService.create(userId, "name1");
        taskService.create(userId, "name2");
        taskService.create(userId2, "name3");
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        Assert.assertEquals(3, taskService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllByProjectId() throws AbstractException {
        @NotNull final TaskDto task1 = new TaskDto();
        task1.setProjectId("project1");
        task1.setName("1");
        @NotNull final TaskDto task2 = new TaskDto();
        task2.setProjectId("project2");
        task2.setName("2");
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.totalCount(userId));
        Assert.assertEquals(1, taskService.findAllByProjectId(userId, "project1").size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllSorted() throws AbstractException {
        taskService.create(userId, "name8");
        taskService.create(userId, "name6");
        taskService.create(userId2, "name3");
        taskService.create(userId2, "name1");
        Assert.assertEquals("name6", taskService.findAll(userId, WBSSort.BY_NAME).get(0).getName());
        Assert.assertEquals("name3", taskService.findAll(userId2, WBSSort.BY_CREATED).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        task.setId("id1");
        task.setName("name1");
        @NotNull final TaskDto task2 = new TaskDto();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals("name1", taskService.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", taskService.findOneById(userId, "id2").getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, "id1"));
        Assert.assertEquals(task.getId(), taskService.removeById(userId, "id1").getId());
        Assert.assertFalse(taskService.existsById(userId, "id1"));
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws AbstractException {
        @NotNull final TaskDto task = new TaskDto();
        task.setId("id1");
        task.setName("name1");
        @NotNull final TaskDto task2 = new TaskDto();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.totalCount(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals("name12", taskService.findOneById(userId, id).getName());
        Assert.assertNotNull(taskService.updateById(userId, id, "name13", "testDescription"));
        Assert.assertEquals("name13", taskService.findOneById(userId, id).getName());
        Assert.assertEquals("testDescription", taskService.findOneById(userId, id).getDescription());
    }

}
